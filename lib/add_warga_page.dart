// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:learn_sqflite/db_warga_helper.dart';
import 'package:learn_sqflite/warga_model.dart';

class AddWargaPage extends StatefulWidget {
  final String? id;
  const AddWargaPage({Key? key, this.id}) : super(key: key);

  @override
  State<AddWargaPage> createState() => _AddWargaPageState();
}

class _AddWargaPageState extends State<AddWargaPage> {
  TextEditingController namaController = TextEditingController();
  TextEditingController alamatController = TextEditingController();
  TextEditingController rtController = TextEditingController();
  TextEditingController rwController = TextEditingController();
  TextEditingController noHpController = TextEditingController();
  TextEditingController pekerjaanController = TextEditingController();
  TextEditingController statusController = TextEditingController();

  Future<void> submitData() async {
    HelperDBWarga.insertWarga(WargaModel(
      nama: namaController.text,
      alamat: alamatController.text,
      rt: rtController.text,
      rw: rwController.text,
      noHp: noHpController.text,
      pekerjaan: pekerjaanController.text,
      status: statusController.text,
    ));

    Navigator.pop(context);
  }

  Future<void> updateData() async {
    HelperDBWarga.updateWarga(WargaModel(
      id: widget.id,
      nama: namaController.text,
      alamat: alamatController.text,
      rt: rtController.text,
      rw: rwController.text,
      noHp: noHpController.text,
      pekerjaan: pekerjaanController.text,
      status: statusController.text,
    ));

    Navigator.pop(context);
  }

  @override
  void initState() {
    super.initState();
    if (widget.id != null) {
      HelperDBWarga.getWargaById(widget.id.toString()).then((value) {
        setState(() {
          namaController.text = value[0].nama.toString();
          alamatController.text = value[0].alamat.toString();
          rtController.text = value[0].rt.toString();
          rwController.text = value[0].rw.toString();
          noHpController.text = value[0].noHp.toString();
          pekerjaanController.text = value[0].pekerjaan.toString();
          statusController.text = value[0].status.toString();
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Tambah Data Warga'),
        ),
        body: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          children: [
            TextField(
              controller: namaController,
              decoration: InputDecoration(
                labelText: 'Nama',
              ),
            ),
            TextField(
              controller: alamatController,
              decoration: InputDecoration(
                labelText: 'Alamat',
              ),
            ),
            TextField(
              controller: rtController,
              decoration: InputDecoration(
                labelText: 'RT',
              ),
            ),
            TextField(
              controller: rwController,
              decoration: InputDecoration(
                labelText: 'RW',
              ),
            ),
            TextField(
              controller: noHpController,
              decoration: InputDecoration(
                labelText: 'No. HP',
              ),
            ),
            TextField(
              controller: pekerjaanController,
              decoration: InputDecoration(
                labelText: 'Pekerjaan',
              ),
            ),
            TextField(
              controller: statusController,
              decoration: InputDecoration(
                labelText: 'Status Pernikahan',
              ),
            ),
            SizedBox(
              height: 16,
            ),
            ElevatedButton(
              onPressed: () {
                // validation
                if (namaController.text.isEmpty ||
                    alamatController.text.isEmpty ||
                    rtController.text.isEmpty ||
                    rwController.text.isEmpty ||
                    noHpController.text.isEmpty ||
                    pekerjaanController.text.isEmpty ||
                    statusController.text.isEmpty) {
                  showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: const Text('Error'),
                        content: const Text('Data tidak boleh kosong'),
                        actions: [
                          TextButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: const Text('OK'),
                          ),
                        ],
                      );
                    },
                  );
                } else {
                  (widget.id == null) ? submitData() : updateData();
                }
              },
              child: Text((widget.id == null) ? 'Tambah' : 'Ubah'),
            ),
          ],
        ));
  }
}
