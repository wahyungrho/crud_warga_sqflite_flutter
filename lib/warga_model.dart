class WargaModel {
  final String? id;
  final String? nama;
  final String? alamat;
  final String? rt;
  final String? rw;
  final String? noHp;
  final String? pekerjaan;
  final String? status;
  final String? createdAt;

  WargaModel({
    this.id,
    this.nama,
    this.alamat,
    this.rt,
    this.rw,
    this.noHp,
    this.pekerjaan,
    this.status,
    this.createdAt,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'nama': nama,
      'alamat': alamat,
      'rt': rt,
      'rw': rw,
      'no_hp': noHp,
      'pekerjaan': pekerjaan,
      'status': status,
      'createdAt': DateTime.now().toString(),
    };
  }

  static WargaModel fromMap(Map<String, dynamic> map) {
    return WargaModel(
      id: map['id'],
      nama: map['nama'],
      alamat: map['alamat'],
      rt: map['rt'],
      rw: map['rw'],
      noHp: map['no_hp'],
      pekerjaan: map['pekerjaan'],
      status: map['status'],
      createdAt: map['createdAt'],
    );
  }

  @override
  String toString() {
    return 'WargaModel{id: $id, nama: $nama, alamat: $alamat, rt: $rt, rw: $rw, noHp: $noHp, pekerjaan: $pekerjaan, status: $status, createdAt: $createdAt}';
  }
}
