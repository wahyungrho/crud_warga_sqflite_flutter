import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:learn_sqflite/warga_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class HelperDBWarga {
  static openDatabaseMethod() async {
    final database = openDatabase(
      join(await getDatabasesPath(), 'warga.db'),
      onCreate: (db, version) {
        return db.execute(
          'CREATE TABLE warga(id INTEGER PRIMARY KEY, nama TEXT, alamat TEXT, rt TEXT, rw TEXT, no_hp TEXT, pekerjaan TEXT, status TEXT, createdAt DATETIME)',
        );
      },
      version: 1,
      onOpen: (db) {
        if (kDebugMode) {
          print('Database opened');
        }
      },
    );

    return database;
  }

  static Future<void> insertWarga(WargaModel warga) async {
    final database = await openDatabaseMethod();

    await database.insert(
      'warga',
      warga.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  static Future<List<WargaModel>> getWarga() async {
    final database = await openDatabaseMethod();

    final List<Map<String, dynamic>> maps = await database.query('warga');

    return List.generate(
        maps.length,
        ((index) => WargaModel(
              id: maps[index]['id'].toString(),
              nama: maps[index]['nama'],
              alamat: maps[index]['alamat'],
              rt: maps[index]['rt'],
              rw: maps[index]['rw'],
              noHp: maps[index]['no_hp'],
              pekerjaan: maps[index]['pekerjaan'],
              status: maps[index]['status'],
              createdAt: DateFormat('yyyy-MM-dd HH:mm:ss').format(
                DateTime.parse(
                    maps[index]['createdAt'] ?? DateTime.now().toString()),
              ),
            )));
  }

  static Future<List<WargaModel>> getWargaById(String id) async {
    final database = await openDatabaseMethod();

    final List<Map<String, dynamic>> maps =
        await database.query('warga', where: 'id = ?', whereArgs: [id]);

    return List.generate(
        maps.length,
        (index) => WargaModel(
              id: maps[index]['id'].toString(),
              nama: maps[index]['nama'],
              alamat: maps[index]['alamat'],
              rt: maps[index]['rt'],
              rw: maps[index]['rw'],
              noHp: maps[index]['no_hp'],
              pekerjaan: maps[index]['pekerjaan'],
              status: maps[index]['status'],
              createdAt: DateFormat('yyyy-MM-dd HH:mm:ss').format(
                DateTime.parse(
                    maps[index]['createdAt'] ?? DateTime.now().toString()),
              ),
            ));
  }

  static Future<void> updateWarga(WargaModel warga) async {
    final database = await openDatabaseMethod();

    await database.update(
      'warga',
      warga.toMap(),
      where: 'id = ?',
      whereArgs: [warga.id],
    );
  }

  static Future<void> deleteWarga(int id) async {
    final database = await openDatabaseMethod();

    await database.delete(
      'warga',
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  static Future<void> deleteAllWarga() async {
    final database = await openDatabaseMethod();
    await database.delete('warga');
  }
}
