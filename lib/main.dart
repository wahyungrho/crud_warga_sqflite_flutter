import 'package:flutter/material.dart';
import 'package:learn_sqflite/add_warga_page.dart';
import 'package:learn_sqflite/db_warga_helper.dart';
import 'package:learn_sqflite/warga_model.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Belajar CRUD SQLite',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<WargaModel>? wargaList = [];

  @override
  void initState() {
    super.initState();
    HelperDBWarga.openDatabaseMethod();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Data Warga RW 06'),
      ),
      body: FutureBuilder<List<WargaModel>>(
        future: HelperDBWarga.getWarga(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            wargaList = snapshot.data;
            return Column(
              children: wargaList!
                  .map((e) => Column(
                        children: [
                          ListTile(
                            title: Text(e.nama!),
                            subtitle: Text(e.alamat!),
                            leading: CircleAvatar(
                              child: Text(e.nama![0]),
                            ),
                            trailing: IconButton(
                              icon: const Icon(Icons.delete),
                              onPressed: () {
                                HelperDBWarga.deleteWarga(
                                    int.parse(e.id.toString()));
                                setState(() {});
                              },
                            ),
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => AddWargaPage(
                                    id: e.id.toString(),
                                  ),
                                ),
                              );
                            },
                          ),
                          (wargaList!.indexOf(e, wargaList!.length - 1) ==
                                  wargaList!.length - 1)
                              ? const SizedBox()
                              : const Divider(
                                  height: 0,
                                  color: Colors.black54,
                                ),
                        ],
                      ))
                  .toList(),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const AddWargaPage(),
            ),
          ).then((value) => setState(() {}));
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
